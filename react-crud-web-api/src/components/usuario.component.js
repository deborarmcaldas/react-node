import React, { Component } from "react";
import UsuarioDataService from "../services/usuario.service";

export default class Usuario extends Component {
  constructor(props) {
    super(props);
    this.onChangeNome = this.onChangeNome.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.getUsuario = this.getUsuario.bind(this);
    this.updateCadastrado = this.updateCadastrado.bind(this);
    this.updateUsuario = this.updateUsuario.bind(this);
    this.deleteUsuario = this.deleteUsuario.bind(this);

    this.state = {
      currentUsuario: {
        id: null,
        nome: "",
        email: "",
        cadastrado: false
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getUsuario(this.props.match.params.id);
  }

  onChangeNome(e) {
    const nome = e.target.value;

    this.setState(function(prevState) {
      return {
        currentUsuario: {
          ...prevState.currentUsuario,
          nome: nome
        }
      };
    });
  }

  onChangeEmail(e) {
    const email = e.target.value;
    
    this.setState(prevState => ({
      currentUsuario: {
        ...prevState.currentUsuario,
        email: email
      }
    }));
  }

  getUsuario(id) {
    UsuarioDataService.get(id)
      .then(response => {
        this.setState({
          currentUsuario: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateCadastrado(status) {
    var data = {
      id: this.state.currentUsuario.id,
      nome: this.state.currentUsuario.nome,
      email: this.state.currentUsuario.email,
      cadastrado: status
    };

    UsuarioDataService.update(this.state.currentUsuario.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentUsuario: {
            ...prevState.currentUsuario,
            cadastrado: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateUsuario() {
    UsuarioDataService.update(
      this.state.currentUsuario.id,
      this.state.currentUsuario
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The usuario was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteUsuario() {    
    UsuarioDataService.delete(this.state.currentUsuario.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/usuarios')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentUsuario } = this.state;

    return (
      <div>
        {currentUsuario ? (
          <div className="edit-form">
            <h4>Usuario</h4>
            <form>
              <div className="form-group">
                <label htmlFor="nome">Nome</label>
                <input
                  type="text"
                  className="form-control"
                  id="nome"
                  value={currentUsuario.nome}
                  onChange={this.onChangeNome}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  value={currentUsuario.email}
                  onChange={this.onChangeEmail}
                />
              </div>

              <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentUsuario.cadastrado ? "Cadastrado" : "Pending"}
              </div>
            </form>

            {currentUsuario.cadastrado ? (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updateCadastrado(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updateCadastrado(true)}
              >
                Publish
              </button>
            )}

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteUsuario}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updateUsuario}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Usuario...</p>
          </div>
        )}
      </div>
    );
  }
}
