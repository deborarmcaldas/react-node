import React, { Component } from "react";
import UsuarioDataService from "../services/usuario.service";

export default class AddUsuario extends Component {
  constructor(props) {
    super(props);
    this.onChangeNome = this.onChangeNome.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.saveUsuario = this.saveUsuario.bind(this);
    this.newUsuario = this.newUsuario.bind(this);

    this.state = {
      id: null,
      nome: "",
      email: "",
      cadastrado: false,

      submitted: false
    };
  }

  onChangeNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  saveUsuario() {
    var data = {
      nome: this.state.nome,
      email: this.state.email
    };

    UsuarioDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          nome: response.data.nome,
          email: response.data.email,
          cadastrado: response.data.cadastrado,

          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newUsuario() {
    this.setState({
      id: null,
      nome: "",
      email: "",
      cadastrado: false,

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newUsuario}>
              Add
            </button>
          </div>
        ) : (
            <div>
              <div className="form-group">
                <label htmlFor="nome">Nome</label>
                <input
                  type="text"
                  className="form-control"
                  id="nome"
                  required
                  value={this.state.nome}
                  onChange={this.onChangeNome}
                  name="nome"
                />
              </div>

              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  required
                  value={this.state.email}
                  onChange={this.onChangeEmail}
                  name="email"
                />
              </div>

              <button onClick={this.saveUsuario} className="btn btn-success">
                Submit
            </button>
            </div>
          )}
      </div>
    );
  }
}
