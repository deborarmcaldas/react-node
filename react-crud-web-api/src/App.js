import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AddTutorial from "./components/add-tutorial.component";
import Tutorial from "./components/tutorial.component";
import TutorialsList from "./components/tutorials-list.component";

import AddUsuario from "./components/add-usuario.component";
import Usuario from "./components/usuario.component";
import UsuariosList from "./components/usuarios-list.component";

class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/tutorials"} className="navbar-brand">
            <Link to={"/usuarios"} className="navbar-brand">
              bezKoder
          </Link>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/tutorials"} className="nav-link">
                  Tutorials
              </Link>
              </li>
              <li className="nav-item">
                <Link to={"/add"} className="nav-link">
                  AddTutorial
              </Link>
              </li>
              <li className="nav-item">
                <Link to={"/usuarios"} className="nav-link">
                  Users
              </Link>
              </li>
              <li className="nav-item">
                <Link to={"/addUser"} className="nav-link">
                  AddUser
              </Link>
              </li>
            </div>
          </Link>
        </nav>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/tutorials"]} component={TutorialsList} />
            <Route exact path={["/", "/usuarios"]} component={UsuariosList} />
            <Route exact path="/add" component={AddTutorial} />
            <Route exact path="/addUser" component={AddUsuario} />
            <Route path="/tutorials/:id" component={Tutorial} />
            <Route path="/usuarios/:id" component={Usuario} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
