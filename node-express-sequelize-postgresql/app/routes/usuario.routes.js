module.exports = app => {
  const usuarios = require("../controllers/usuario.controller.js");

  var router = require("express").Router();

  // Criar um novo Usuario
  router.post("/", usuarios.create); 

  // Retrieve all Usuarios
  router.get("/", usuarios.findAll);

  // Retrieve all published Usuarios
  // router.get("/published", usuarios.findAllPublished); 

  // Retrieve a single Usuario with id
  router.get("/:id", usuarios.findOne); 

  // Update a Usuario with id
  router.put("/:id", usuarios.update); 

  // Delete a Usuario with id
  router.delete("/:id", usuarios.delete); 

  // Create a new Usuario
  router.delete("/", usuarios.deleteAll); 

  app.use("/api/usuarios", router);
};
