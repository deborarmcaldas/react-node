module.exports = (sequelize, Sequelize) => {
  const Usuario = sequelize.define("usuario", {
    nome: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    cadastrado: {
      type: Sequelize.BOOLEAN
    }
  });

  return Usuario;
};